#include "depth_buffer.h"

DepthBuffer::DepthBuffer(int width, int height) : width(width), height(height) {
    data = new float*[width];

    for (int x = 0; x < width; x++) {
        data[x] = new float[height];
    }

    clear();
}

DepthBuffer::~DepthBuffer() {
    for (int x = 0; x < width; x++) {
        delete [] data[x];
    }

    delete [] data;
}

float DepthBuffer::get(int x, int y) {
    return data[x][y];
}

void DepthBuffer::put(int x, int y, float value) {
    data[x][y] = value;
}

void DepthBuffer::clear() {
    for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
            data[x][y] = 1;
        }
    }
}
