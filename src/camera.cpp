#include "camera.h"

#include <constants.hpp>

Camera::Camera() = default;

Camera::Camera(glm::vec3 position) : position(position) { }

void Camera::rotateHorizontally(float angle) {
    horizontalAngle += angle;

    horizontalAngle = std::fmod(horizontalAngle, piksel::PI * 2);
}

void Camera::rotateVertically(float angle) {
    verticalAngle += angle;

    if (verticalAngle > piksel::PI / 2) verticalAngle = piksel::PI / 2;
    else if (verticalAngle < -piksel::PI / 2) verticalAngle = -piksel::PI / 2;
}

void Camera::updateDirection() {
    float projection = std::cos(verticalAngle);
    forward = glm::vec3(projection * std::sin(horizontalAngle), std::sin(verticalAngle), -projection * std::cos(horizontalAngle));

    right = glm::vec3(std::sin(horizontalAngle + piksel::PI / 2), 0, -std::cos(horizontalAngle + piksel::PI / 2));

    projection = std::cos(verticalAngle + piksel::PI / 2);
    up = glm::vec3(projection * std::sin(horizontalAngle), std::sin(verticalAngle + piksel::PI / 2), -projection * std::cos(horizontalAngle));
}

