#pragma once

#include <piksel/baseapp.hpp>

#include "camera.h"
#include "objects/object3d.h"
#include "objects/cube.h"
#include "objects/plane.h"

class App : public piksel::BaseApp {

private:

    std::map<int, bool> pressedKeys;

    float aspect;

    float t = 0;

    float dt = 0;

    Camera camera;

    DepthBuffer depthBuffer;

    glm::mat4 projectionViewMatrix;

    Camera directionalLightCamera;

    DepthBuffer directionalLightDepthBuffer;

    glm::mat4 directionalLightMatrix;

    Plane plane = Plane(glm::vec3(0.8, 0.8, 1));

    Cube cube;

    Object3D* objects[2] = { &plane, &cube };

public:

    App();

    void setup() override;

    void keyPressed(int key) override;

    void keyReleased(int key) override;

    void draw(piksel::Graphics& g) override;

private:

    void updateTime();

    void updateControls();

    void updateProjectionViewMatrix();

    void updateScene();

    void rotateObject(Object3D& object, glm::vec3 angularVelocity) const;

    void drawShadows();

    void drawScene(piksel::Graphics& g);

};
