#pragma once

#include <glm/glm.hpp>

class Camera {

public:

    glm::vec3 position = glm::vec3(0, 0, 0);

    glm::vec3 forward;

    glm::vec3 right;

    glm::vec3 up;

private:

    float horizontalAngle = 0;

    float verticalAngle = 0;

public:

    Camera();

    explicit Camera(glm::vec3 position);

    void rotateHorizontally(float angle);

    void rotateVertically(float angle);

    void updateDirection();

};
