#pragma once

#include "object3d.h"

class Plane : public Object3D {

public:

    explicit Plane(glm::vec3 color);

};
