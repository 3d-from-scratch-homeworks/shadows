#pragma once

#include "../graphics_options.h"

/**
 * The polygon that must be convex with all vertices being in one plane
 */
class Polygon3D {

public:

    std::vector<glm::vec3> vertices;

    glm::vec3 color;

public:

    Polygon3D(glm::vec3 color, glm::vec3 a, glm::vec3 b, glm::vec3 c);

    Polygon3D(glm::vec3 color, glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 d);

    void draw(GraphicsOptions options);

private:

    static bool isVisible(std::vector<glm::vec4>& projectedVertices);

    static int convertToScreenX(GraphicsOptions& options, glm::vec4 projectedVertex);

    static int convertToScreenY(GraphicsOptions& options, glm::vec4 projectedVertex);

    static glm::vec3 calculateNormal(std::vector<glm::vec4>& transformedVertices);

    /**
     * Performs the perspective interpolation between two values
     * In case of the orthogonal projection is identical to the linear interpolation
     * See the link for more information:
     * https://webglfundamentals.org/webgl/lessons/webgl-3d-perspective-correct-texturemapping.html
     */
    template<typename T> static T perspectiveInterpolation(T a, float aW, T b, float bW, float t);

    static bool isInShadow(glm::vec3 position, GraphicsOptions& options);

    static glm::vec3 calculateLightColor(glm::vec3 position, glm::vec3 normal, GraphicsOptions& options);

};
