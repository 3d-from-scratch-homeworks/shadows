#include "plane.h"

// The length of the half of the plane edge
const float A = 0.5;

Plane::Plane(glm::vec3 color) {
    glm::vec3 a(-A, 0, A);
    glm::vec3 b(A, 0, A);
    glm::vec3 c(A, 0, -A);
    glm::vec3 d(-A, 0, -A);

    polygons.emplace_back(color, a, b, c, d);
}
