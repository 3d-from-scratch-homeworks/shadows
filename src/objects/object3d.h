#pragma once

#include "polygon3d.h"

class Object3D {

public:

    std::vector<Polygon3D> polygons;

    glm::vec3 position = glm::vec3(0, 0, 0);

    glm::vec3 rotation = glm::vec3(0, 0, 0);

    glm::vec3 scale = glm::vec3(1, 1, 1);

public:

    Object3D();

    explicit Object3D(glm::vec3 position);

    Object3D(glm::vec3 position, glm::vec3 rotation);

    Object3D(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale);

    void draw(GraphicsOptions options);

    glm::mat4 getModelMatrix() const;

};
