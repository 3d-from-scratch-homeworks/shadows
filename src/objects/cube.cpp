#include "cube.h"

// The cube radius
const float R = 1;

// The length of the half of the cube edge
const float A = R / std::sqrt(3);

Cube::Cube() {
    glm::vec3 a(-A, -A, A);
    glm::vec3 b(A, -A, A);
    glm::vec3 c(A, -A, -A);
    glm::vec3 d(-A, -A, -A);

    glm::vec3 e(-A, A, A);
    glm::vec3 f(A, A, A);
    glm::vec3 g(A, A, -A);
    glm::vec3 h(-A, A, -A);

    polygons.emplace_back(glm::vec3(1, 0, 0), a, d, c, b);
    polygons.emplace_back(glm::vec3(0, 1, 0), e, f, g, h);
    polygons.emplace_back(glm::vec3(0, 0, 1), a, b, f, e);
    polygons.emplace_back(glm::vec3(1, 1, 0), b, c, g, f);
    polygons.emplace_back(glm::vec3(0, 1, 1), c, d, h, g);
    polygons.emplace_back(glm::vec3(1, 0, 1), d, a, e, h);
}
