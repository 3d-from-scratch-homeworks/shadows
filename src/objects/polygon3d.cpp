#include "polygon3d.h"

#include <glm/gtx/compatibility.hpp>

Polygon3D::Polygon3D(glm::vec3 color, glm::vec3 a, glm::vec3 b, glm::vec3 c) : color(color) {
    vertices.insert(vertices.end(), { a, b, c });
}

Polygon3D::Polygon3D(glm::vec3 color, glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 d) : color(color) {
    vertices.insert(vertices.end(), { a, b, c, d });
}

void Polygon3D::draw(GraphicsOptions options) {
    // Transform the vertices using the model matrix
    std::vector<glm::vec4> transformedVertices;

    for (glm::vec3& vertex : vertices) {
        transformedVertices.push_back(options.modelMatrix * glm::vec4(vertex, 1));
    }

    // Project the vertices using the projection-view matrix
    std::vector<glm::vec4> projectedVertices;

    for (glm::vec4& transformedVertex : transformedVertices) {
        glm::vec4 projectedVertex = options.projectionViewMatrix * transformedVertex;

        // Since the clipping is not implemented it is better to hide the objects that are partially behind the camera
        // to avoid incorrect results when using the perspective projection
        if (projectedVertex.w <= 0) return;

        projectedVertices.push_back(projectedVertex);
    }

    // Don't draw the polygon if the front side isn't visible
    if (!isVisible(projectedVertices)) return;

    glm::vec3 normal = calculateNormal(transformedVertices);

    // Find the polygon bounds in the y-axis
    int minY = std::numeric_limits<int>::max();
    int maxY = std::numeric_limits<int>::min();

    for (glm::vec4& projectedVertex : projectedVertices) {
        int y = convertToScreenY(options, projectedVertex);

        if (y < minY) minY = y;
        if (y > maxY) maxY = y;
    }

    if (minY < 0) minY = 0;
    if (maxY >= options.height) maxY = options.height - 1;

    transformedVertices.emplace_back(transformedVertices[0]);
    transformedVertices.emplace_back(transformedVertices[1]);

    projectedVertices.emplace_back(projectedVertices[0]);
    projectedVertices.emplace_back(projectedVertices[1]);

    // Draw each horizontal segment of the polygon
    for (int y = minY; y <= maxY; y++) {
        int index = 0;

        int intersections[2];
        glm::vec3 transformedPoints[2];
        glm::vec4 projectedPoints[2];

        // Find the intersection points of the horizontal line and the polygon edges
        for (int i = 0; i < vertices.size(); i++) {
            glm::vec4& at = transformedVertices[i];
            glm::vec4& bt = transformedVertices[i + 1];

            glm::vec4& ap = projectedVertices[i];
            glm::vec4& bp = projectedVertices[i + 1];
            glm::vec4& cp = projectedVertices[i + 2];

            int aX = convertToScreenX(options, ap);
            int aY = convertToScreenY(options, ap);

            int bX = convertToScreenX(options, bp);
            int bY = convertToScreenY(options, bp);

            int cY = convertToScreenY(options, cp);

            if (y < aY && y < bY || y > aY && y > bY) continue;

            if (y == bY) {
                if (y < aY && y < cY || y > aY && y > cY) {
                    intersections[0] = bX;
                    intersections[1] = bX;

                    transformedPoints[0] = bt;
                    transformedPoints[1] = bt;

                    projectedPoints[0] = bp;
                    projectedPoints[1] = bp;
                }
                else {
                    intersections[index] = bX;
                    transformedPoints[index] = bt;
                    projectedPoints[index] = bp;

                    index++;
                }
            }
            else if (y == aY) {
                continue;
            }
            else {
                float t = (float) (y - aY) / (float) (bY - aY);

                intersections[index] = (int) std::round(glm::lerp((float) aX, (float) bX, t));
                transformedPoints[index] = perspectiveInterpolation(at, ap.w, bt, bp.w, t);
                projectedPoints[index] = perspectiveInterpolation(ap, ap.w, bp, bp.w, t);

                index++;
            }

            if (index > 1) break;
        }

        // Sort the intersection points by x value
        if (intersections[0] > intersections[1]) {
            std::swap(intersections[0], intersections[1]);
            std::swap(transformedPoints[0], transformedPoints[1]);
            std::swap(projectedPoints[0], projectedPoints[1]);
        }

        int minX = std::max(intersections[0], 0);
        int maxX = std::min(intersections[1], options.width - 1);

        // Draw each pixel of the horizontal segment
        for (int x = minX; x <= maxX; x += 1) {
            float t = (float) (x - intersections[0]) / (float) (intersections[1] - intersections[0]);

            glm::vec4 projectedPoint = perspectiveInterpolation(projectedPoints[0], projectedPoints[0].w, projectedPoints[1], projectedPoints[1].w, t);
            float z = projectedPoint.z / projectedPoint.w;

            // Perform the depth test
            if (z >= -1 && z < options.depthBuffer->get(x, y)) {
                if (options.graphics) {
                    glm::vec3 position = perspectiveInterpolation(transformedPoints[0], projectedPoints[0].w, transformedPoints[1], projectedPoints[1].w, t);

                    glm::vec3 lightColor = calculateLightColor(position, normal, options);

                    // Draw to the screen and the depth buffer when drawing the scene
                    options.graphics->stroke(glm::vec4(color * lightColor, 1));
                    options.graphics->point((float) x, (float) y);
                    options.depthBuffer->put(x, y, z);
                }
                else {
                    // Draw only to the depth buffer when drawing the shadows
                    options.depthBuffer->put(x, y, z);
                }
            }
        }
    }
}

bool Polygon3D::isVisible(std::vector<glm::vec4>& projectedVertices) {
    glm::vec4 last = projectedVertices[projectedVertices.size() - 1];
    last /= last.w;

    glm::vec4 first = projectedVertices[0];
    first /= first.w;

    glm::vec4 second = projectedVertices[1];
    second /= second.w;

    glm::vec3 a = second - first;
    glm::vec3 b = last - first;

    glm::vec3 normal = glm::normalize(glm::cross(a, b));

    float cos = glm::dot(normal, glm::vec3(0, 0, -1));

    return cos < 0;
}

int Polygon3D::convertToScreenX(GraphicsOptions& options, glm::vec4 projectedVertex) {
    return (int) std::round((projectedVertex.x / projectedVertex.w + 1) / 2 * (float) options.width);
}

int Polygon3D::convertToScreenY(GraphicsOptions& options, glm::vec4 projectedVertex) {
    return (int) std::round((1 - projectedVertex.y / projectedVertex.w) / 2 * (float) options.height);
}

glm::vec3 Polygon3D::calculateNormal(std::vector<glm::vec4>& transformedVertices) {
    glm::vec3 a = transformedVertices[1] - transformedVertices[0];
    glm::vec3 b = transformedVertices[transformedVertices.size() - 1] - transformedVertices[0];

    return glm::normalize(glm::cross(a, b));
}

template <typename T> T Polygon3D::perspectiveInterpolation(T a, float aW, T b, float bW, float t) {
    return ((1 - t) * a / aW + t * b / bW) / ((1 - t) / aW + t / bW);
}

bool Polygon3D::isInShadow(glm::vec3 position, GraphicsOptions& options) {
    glm::vec4 transformedPosition = options.directionalLightMatrix * glm::vec4(position, 1);
    transformedPosition /= transformedPosition.w;

    int width = options.directionalLightDepthBuffer->width;
    int height = options.directionalLightDepthBuffer->height;

    int x = (int) std::round((transformedPosition.x + 1) / 2 * (float) width);
    int y = (int) std::round((1 - transformedPosition.y) / 2 * (float) height);

    if (x < 0 || x >= width || y < 0 || y >= height) return false;

    // The 0.05 value is a bias that helps to avoid artifacts caused by the limited precision of the depth buffer
    return transformedPosition.z > options.directionalLightDepthBuffer->get(x, y) + 0.05;
}

glm::vec3 Polygon3D::calculateLightColor(glm::vec3 position, glm::vec3 normal, GraphicsOptions& options) {
    glm::vec3 lightColor = options.ambientLightColor;

    if (!isInShadow(position, options)) {
        float lighting = std::max(-glm::dot(options.directionalLightDirection, normal), 0.0f);
        lightColor += lighting * options.directionalLightColor;
    }

    return lightColor;
}
