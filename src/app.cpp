#include "app.h"

#include <glm/ext.hpp>

const float FOV_Y = 70;

const float CAMERA_ROTATION_SPEED = 60;

const float CAMERA_MOVEMENT_SPEED = 10;

const int SHADOW_DEPTH_BUFFER_SIZE = 1024;

App::App() : piksel::BaseApp(640, 480, "Shadows"),
    aspect((float) width / (float) height),
    depthBuffer(width, height),
    directionalLightDepthBuffer(SHADOW_DEPTH_BUFFER_SIZE, SHADOW_DEPTH_BUFFER_SIZE) { }

void App::setup() {
    camera.position = glm::vec3(0, 2, 10);

    directionalLightCamera.position.y = 10;
    directionalLightCamera.rotateHorizontally(glm::radians(-45.0f));
    directionalLightCamera.rotateVertically(glm::radians(-60.0f));
    directionalLightCamera.updateDirection();

    directionalLightMatrix = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, 1.0f, 20.0f)
        * glm::lookAt(directionalLightCamera.position, directionalLightCamera.position + directionalLightCamera.forward, directionalLightCamera.up);

    plane.scale = glm::vec3(10, 1, 10);

    cube.position.y = 2;
}

void App::keyPressed(int key) {
    pressedKeys[key] = true;
}

void App::keyReleased(int key) {
    pressedKeys[key] = false;
}

void App::draw(piksel::Graphics& g) {
    updateTime();
    updateControls();
    updateProjectionViewMatrix();
    updateScene();

    drawShadows();
    drawScene(g);
}

void App::updateTime() {
    float nt = (float) millis() / 1000;
    dt = nt - t;
    t = nt;
}

void App::updateControls() {
    if (pressedKeys[GLFW_KEY_LEFT]) camera.rotateHorizontally(-glm::radians(CAMERA_ROTATION_SPEED) * dt);
    else if (pressedKeys[GLFW_KEY_RIGHT]) camera.rotateHorizontally(glm::radians(CAMERA_ROTATION_SPEED) * dt);

    if (pressedKeys[GLFW_KEY_DOWN]) camera.rotateVertically(-glm::radians(CAMERA_ROTATION_SPEED) * dt);
    else if (pressedKeys[GLFW_KEY_UP]) camera.rotateVertically(glm::radians(CAMERA_ROTATION_SPEED) * dt);

    if (pressedKeys[GLFW_KEY_A]) camera.position -= camera.right * CAMERA_MOVEMENT_SPEED * dt;
    else if (pressedKeys[GLFW_KEY_D]) camera.position += camera.right * CAMERA_MOVEMENT_SPEED * dt;

    if (pressedKeys[GLFW_KEY_S]) camera.position -= camera.forward * CAMERA_MOVEMENT_SPEED * dt;
    else if (pressedKeys[GLFW_KEY_W]) camera.position += camera.forward * CAMERA_MOVEMENT_SPEED * dt;

    if (pressedKeys[GLFW_KEY_F]) camera.position.y -= CAMERA_MOVEMENT_SPEED * dt;
    else if (pressedKeys[GLFW_KEY_R]) camera.position.y += CAMERA_MOVEMENT_SPEED * dt;

    camera.updateDirection();
}

void App::updateProjectionViewMatrix() {
    glm::mat4 projectionMatrix = glm::perspective(glm::radians(FOV_Y), aspect, 0.1f, 100.0f);
    glm::vec3 center = camera.position + camera.forward;
    glm::mat4 viewMatrix = glm::lookAt(camera.position, center, camera.up);
    projectionViewMatrix = projectionMatrix * viewMatrix;
}

void App::updateScene() {
    glm::vec3 angularVelocity = glm::radians(glm::vec3(10, 10, 0));
    rotateObject(cube, angularVelocity);
}

void App::rotateObject(Object3D& object, glm::vec3 angularVelocity) const {
    object.rotation = object.rotation + angularVelocity * dt;
    object.rotation = glm::mod(object.rotation, piksel::PI * 2);
}

void App::drawShadows() {
    GraphicsOptions options(SHADOW_DEPTH_BUFFER_SIZE, SHADOW_DEPTH_BUFFER_SIZE);
    options.depthBuffer = &directionalLightDepthBuffer;
    options.projectionViewMatrix = directionalLightMatrix;

    directionalLightDepthBuffer.clear();

    for (Object3D* object : objects) {
        object->draw(options);
    }
}

void App::drawScene(piksel::Graphics& g) {
    GraphicsOptions options(width, height);
    options.graphics = &g;
    options.depthBuffer = &depthBuffer;
    options.projectionViewMatrix = projectionViewMatrix;
    options.ambientLightColor = glm::vec3(0.2);
    options.directionalLightColor = glm::vec3(0.8);
    options.directionalLightDirection = directionalLightCamera.forward;
    options.directionalLightDepthBuffer = &directionalLightDepthBuffer;
    options.directionalLightMatrix = directionalLightMatrix;

    g.background(glm::vec4(0, 0, 0, 1));
    depthBuffer.clear();

    for (Object3D* object : objects) {
        object->draw(options);
    }
}
