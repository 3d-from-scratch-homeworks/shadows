#pragma once

#include <piksel/graphics.hpp>

#include "depth_buffer.h"

class GraphicsOptions {

public:

    int width;

    int height;

    piksel::Graphics* graphics = nullptr;

    DepthBuffer* depthBuffer = nullptr;

    glm::mat4 modelMatrix;

    glm::mat4 projectionViewMatrix;

    glm::vec3 ambientLightColor;

    glm::vec3 directionalLightColor;

    glm::vec3 directionalLightDirection;

    DepthBuffer* directionalLightDepthBuffer = nullptr;

    glm::mat4 directionalLightMatrix;

public:

    GraphicsOptions(int width, int height);

};
