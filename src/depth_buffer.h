#pragma once

/**
 * The depth buffer with the values from -1 to 1
 */
class DepthBuffer {

public:

    const int width;

    const int height;

private:

    float** data;

public:

    DepthBuffer(int width, int height);

    ~DepthBuffer();

    float get(int x, int y);

    void put(int x, int y, float value);

    void clear();

};
